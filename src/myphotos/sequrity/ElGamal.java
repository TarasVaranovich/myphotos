package myphotos.sequrity;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

public class ElGamal {
	
	private BigInteger p;
	private BigInteger b;
	private BigInteger c;
	private BigInteger secretKey;
	
	public ElGamal() {
		
		secretKey = new BigInteger("1234");		
		Random sc = new SecureRandom();
        p = BigInteger.probablePrime(64, sc);
        b = new BigInteger("3");
        c = b.modPow(secretKey, p);
	}
	
	public Integer decript(BigInteger brmodp, BigInteger EC, BigInteger secretKeyDe, BigInteger pDe) {
		
		Integer message = 0;
		
		BigInteger crmodp = brmodp.modPow(secretKeyDe, pDe);
        BigInteger d = crmodp.modInverse(pDe);
        BigInteger ad = d.multiply(EC).mod(pDe);
		
        message = ad.intValue();
        
		return message;
	}

	public BigInteger getpKey() {
		return p;
	}
	
	public BigInteger getbKey() {
		return b;
	}
	
	public BigInteger getcKey() {
		return c;
	}
	
	public BigInteger getSecretKey() {
		return secretKey;
	}
			
}
