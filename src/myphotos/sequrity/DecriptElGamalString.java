package myphotos.sequrity;

import java.math.BigInteger;

public class DecriptElGamalString {
	
	public static String decriptString(String basicString, BigInteger secretKey, BigInteger gamalP) {
		
		String[] array = basicString.split(",");
		
		String resultStr = "";
		
		for(int i = 0; i < array.length; i++) {
			
			String[] cypherText = array[i].split(":");
			ElGamal EG2 = new ElGamal();
			BigInteger gaK = new BigInteger(cypherText[0]);//brmodp
			BigInteger gbK = new BigInteger(cypherText[1]); // EC
			System.out.println("->gA->" + gaK + "gB->" + gbK);
			int res = EG2.decript(gaK, gbK, secretKey, gamalP);
			System.out.println(res);		
			resultStr += (char)res;

		}
		
		return resultStr;
	}
}
