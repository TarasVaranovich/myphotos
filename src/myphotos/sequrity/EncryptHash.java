package myphotos.sequrity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptHash {
	
	public static byte[] encriptSHA1 (String basicPassword) {
	
		try {
			MessageDigest d = MessageDigest.getInstance("sha-1");
			d.update(basicPassword.getBytes());
			
			return d.digest();
			
		} catch (NoSuchAlgorithmException nsae) {
			
			return null;
		}				
	}
}
