package myphotos.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.IOException;
import java.security.SecureRandom;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import myphotos.struct.Photo;
//import myphotos.data.GetPhotosArray;
import myphotos.sequrity.EncryptHash;
import myphotos.data.AddPhotos;
import myphotos.navigation.UserNavigation;
import myphotos.navigation.UserNavigationSafety;


@Controller
@RequestMapping("/addphotos.htm")
public class AddPhotosController {
		
	//variables to store in Redis
		String token;
		byte[] userNameTmp = EncryptHash.encriptSHA1("Taras");
	//may bee unsuccessful architecture ->
		private String addPhotosUUIDTmp;	
	
	@ModelAttribute
	public void setVaryResponseHeader(HttpServletResponse response) {
		response.setHeader("Content-Type", "text/plain;charset=UTF-8");
	}
	
	@RequestMapping(method = GET)
	public String photosForm(Model model,
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) {
		System.out.println("AddPhotos get request 1:" + uuid);
		System.out.println("AddPhotos get request 2:" + key);

		if(UserNavigation.comparePhotosToken(uuid, key)) {
			
			this.addPhotosUUIDTmp = uuid;
			
			return "pages/addphotos.html";
			
		} else {
			
			return "pages/wrongpage.html";
			
		}
	}
	
	@RequestMapping(value="usr.htm",method = POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String photosUserKey(Model model) {		
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		token = bytes.toString();
	    ObjectMapper mapperUser = new ObjectMapper();
	    ObjectNode userNode = mapperUser.createObjectNode();
	    userNode.put("token",token);
	    userNode.put("uuid",this.addPhotosUUIDTmp);
	    UserNavigation.setAddPhotosToken(this.addPhotosUUIDTmp, token);
	    
		return userNode.toString();
	}
	
	
	@RequestMapping(value="add.htm",method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	public String addphotos(@RequestBody String photoDataJSON,
													Model model,
	@RequestParam(value = "uuid", required = true) String uuid, 
	@RequestParam(value = "key", required = true) String key) throws 
											JsonProcessingException, IOException { 
			
			System.out.println("Add Photos UUID:" + uuid);
			System.out.println("Add photos key:" + key);
			
			if(UserNavigation.compareAddPhotosToken(uuid, key)) {
				ObjectMapper mapper = new ObjectMapper();		
				Photo[] newPhotos = mapper.readValue(photoDataJSON, Photo[].class);
				AddPhotos af = new AddPhotos(newPhotos, 
								UserNavigation.getUserName(this.addPhotosUUIDTmp));
				af.add();
				
				return "true";
				
			} else {
				
				return "false";
			}	
	}
	
	@RequestMapping(value="exitfromaddphotos.htm",method = GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String exitFromAddPhotos(Model model, 
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) { 
		Boolean exitMarker = false;
		System.out.println("Exit from add photos uuid:" + uuid);
		System.out.println("Exit from add photos server key:" + key);
		UserNavigationSafety uns = new UserNavigationSafety();
		if(uns.compareAddPhotosToken(uuid, key)) {
			
			if(uns.deleteUser(uuid)) {
				System.out.println("User deleted. LOG OUT.");
				exitMarker = true;
			}
			
		} else {
			System.out.println("User NOT deleted.");
			exitMarker = false;
		}
		
		return exitMarker.toString();
	}
}
