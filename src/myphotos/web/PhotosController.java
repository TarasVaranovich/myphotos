package myphotos.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


import java.security.SecureRandom;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.File;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RequestHeader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import myphotos.converters.HTMLtoPDFConverter;
import myphotos.data.GetPhotoByOrder;
import myphotos.data.DeletePhotoByID;
import myphotos.struct.PhotoDB;
import myphotos.struct.CreateDoc;
import myphotos.struct.DocDataFile;
import myphotos.converters.PDFtoBase64Converter;
import myphotos.data.AddDocument;
import myphotos.struct.DeletePhotos;
import myphotos.navigation.UserNavigation;
import myphotos.navigation.UserNavigationSafety;

@Controller
@RequestMapping("/photos.htm")
public class PhotosController {
	
	//may bee unsuccessful architecture ->
	private String photosUUIDTmp;
	String documentName = "";
	
	
	@ModelAttribute
	public void setVaryResponseHeader(HttpServletResponse response) {
		response.setHeader("Content-Type", "text/plain;charset=UTF-8");
	}		

	@RequestMapping(method = GET)
	public String photosForm(Model model, 
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) {
			

		System.out.println("Photos get request uuid:" + uuid);
		System.out.println("Photos get request key:" + key);
		
		
		if(UserNavigation.compareLoginToken(uuid, key) || 
				UserNavigation.compareRegistrationToken(uuid, key)) {
			
			photosUUIDTmp = uuid;
			
			return "pages/photos.html";
			
		} else {
			
			return "pages/wrongpage.html";
			
		}
	}
	
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	@RequestMapping(value="getphotos.htm",method = GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String photosGetContent(Model model, 
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key,
			@RequestHeader(value="photonumber") Integer photoNumber) {	
		System.out.println("GET PHOTOS FROM " + photoNumber + " REQUEST.");
		System.out.println("Get photos UUID:" + uuid);
		System.out.println("GET photos Key:" + key);
		System.out.println("Photo number:" + photoNumber);
		ObjectMapper mapperPhoto = new ObjectMapper();
		ObjectNode photoNode = mapperPhoto.createObjectNode();
		UserNavigationSafety uns = new UserNavigationSafety();
			
		if(uns.comparePhotosToken(uuid, key)) {
			System.out.println("Get photos routnig complete");
			GetPhotoByOrder gfbo = new GetPhotoByOrder(uns.getUserName(uuid),  photoNumber);
			PhotoDB currentPhoto = gfbo.getPhoto();
			if(currentPhoto != null) {
				
				photoNode.put("photoname",currentPhoto.getName());
				photoNode.put("photobin", "data:image/jpeg;base64," + currentPhoto.getBinary());
				photoNode.put("photodate", currentPhoto.getDate());
				photoNode.put("photodesc", currentPhoto.getDescription());
				photoNode.put("photoid", currentPhoto.getID());
				
				return photoNode.toString();
				
			} else {
				return "";
			}	
		} else {
			return "";
		}
	}
	
	@RequestMapping(value="usr.htm",method = POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String photosUserKey(Model model) {		
		System.out.println("POST USR..");
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		String token = bytes.toString();

		UserNavigation.setPhotosToken(this.photosUUIDTmp, token);
	    ObjectMapper mapperUser = new ObjectMapper();
	    ObjectNode userNode = mapperUser.createObjectNode();
	    userNode.put("token",token);
	    userNode.put("uuid", this.photosUUIDTmp);
	    userNode.put("name", UserNavigation.getUserNameString(this.photosUUIDTmp));
	    
		return userNode.toString();
	}
	
	@RequestMapping(value="createdoc.htm",method = POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String createDoc(@RequestBody String documentJson, 
							Model model, 
							@RequestParam(value = "uuid", required = true) String uuid, 
							@RequestParam(value = "key", required = true) String key) {		
		System.out.println("Create Doc UUID:" + uuid);
		System.out.println("Create Doc Key:" + key);
		Boolean complete = true;
		ObjectMapper docMapper = new ObjectMapper();
		if(UserNavigation.comparePhotosToken(uuid, key)) {
			try {
				CreateDoc newDoc = docMapper.readValue(documentJson, CreateDoc.class);
				newDoc.setUUID(uuid);
				this.documentName = newDoc.getDocName();
				//
				if(newDoc.createImages()) {
					if(newDoc.createHTMLPage()) {
						if(HTMLtoPDFConverter.convert(DocDataFile.getTempFilesPath()
													+ uuid 
													+ ".html", 
													DocDataFile.getTempFilesPath() 
													+ uuid 
													+ ".pdf")){
							newDoc.deleteTempFiles();
							complete = true;
							
						} else {
							complete = false;
							
						}
					}
				}
				
				//
			} catch(IOException ioe) {
				
				ioe.printStackTrace(); 
			} 
		}
		if(complete) {
			
			return "publicpages/pdfpreview.html";
			
		} else {
			
			return "publicpages/pdfwrongpage.html";
			
		}
	}
	
	@RequestMapping(value="preview.htm",method = POST)
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	public String returnPDF(Model model) { 
		System.out.println("Preview test");
		String pdfFile = PDFtoBase64Converter.convert(DocDataFile.getTempFilesPath() 
																			+ this.photosUUIDTmp 
																			+ ".pdf");
		
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		String tokenPreview = bytes.toString();
		String token = bytes.toString();
	    
		ObjectMapper mapperDoc = new ObjectMapper();
		ObjectNode docNode = mapperDoc.createObjectNode();
		docNode.put("token",tokenPreview);
		docNode.put("uuid", this.photosUUIDTmp);
		docNode.put("pdfcontent", pdfFile);
		docNode.put("docname", this.documentName);
		UserNavigation.setPdfPreviewToken(this.photosUUIDTmp, token);
		
		return docNode.toString();
	}
	
	@RequestMapping(value="savedocument.htm",method = POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String saveDocument(Model model,
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) { 
		
		System.out.println("SAVE");
		System.out.println("Save document uuid:" + uuid);
		System.out.println("Save document key:" + key);
		AddDocument addd = new AddDocument(DocDataFile.getTempFilesPath() 
														+ this.photosUUIDTmp 
														+ ".pdf",
														  this.documentName, 	
														  UserNavigation.getUserName(uuid));
		if(UserNavigation.comparePdfPreviewToken(uuid, key)) {
			if(addd.Add()) {
				
				File file = new File(DocDataFile.getTempFilesPath() 
												+ this.photosUUIDTmp 
												+ ".pdf",
												this.documentName);
				file.delete();
				
				return "true";
			} else {
				System.out.println("Document NOT added...");
				return "false";
			}
		} else {
			return "false";
		}
	}
	
	@RequestMapping(value="removedocument.htm",method = GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Boolean removeDocument(Model model, 
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) { 
		System.out.println("Remvoe doc from server document uuid:" + uuid);
		System.out.println("Remove doc from server key:" + key);
		
		if(UserNavigation.comparePdfPreviewToken(uuid, key) ||
				UserNavigation.comparePhotosToken(uuid, key)) {
			File file = new File(DocDataFile.getTempFilesPath() 
											+ this.photosUUIDTmp 
											+ ".pdf");
			file.delete();
			
			return true;
		} else {
			return false;
		}
	}
	
	@RequestMapping(value="deletephotos.htm",method = POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String deletePhotos(@RequestBody String photosJson, Model model, 
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) {
		System.out.println("Delete photos UUID:" + uuid);
		System.out.println("Delete photos key:" + key);
		Boolean deleteMarker = true;	
		ObjectMapper deletePhotosMapper = new ObjectMapper();
		if(UserNavigation.comparePhotosToken(uuid, key)) {
			try {
				DeletePhotos newDeletePhotos = deletePhotosMapper.readValue(photosJson, 
																	DeletePhotos.class);
				for(Integer n = 0; n < newDeletePhotos.getPhotoIDS().length; n++ ) {
					DeletePhotoByID dfbid = new DeletePhotoByID(newDeletePhotos.getPhotoIDS()[n]);
					if(dfbid.delete()) {
						deleteMarker = true;
					} else {
						deleteMarker = false;
					}
				}
			} catch(IOException ioe) {
				
				ioe.printStackTrace(); 
			} 
		}
		return deleteMarker.toString();
	}
	
	@RequestMapping(value="exitfromphotos.htm",method = GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String exitFromPhotos(Model model, 
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) { 
		Boolean exitMarker = false;
		System.out.println("Exit from photos uuid:" + uuid);
		System.out.println("Exit from photos server key:" + key);
		UserNavigationSafety uns = new UserNavigationSafety();
		if(uns.comparePhotosToken(uuid, key)) {
			
			if(uns.deleteUser(uuid)) {
				System.out.println("User deleted. LOG OUT.");
				exitMarker = true;
			}
			
		} else {
			
			exitMarker = false;
		}
		
		return exitMarker.toString();
	}
}
