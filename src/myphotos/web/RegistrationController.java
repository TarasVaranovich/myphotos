package myphotos.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.UUID;
import java.math.BigInteger;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.core.JsonProcessingException;

import myphotos.struct.User;
import myphotos.sequrity.ElGamal;
import myphotos.sequrity.DecriptElGamalString;
import myphotos.sequrity.EncryptHash;
import myphotos.data.CreateNewUser;
import myphotos.navigation.UserNavigation;
import myphotos.data.CheckUserIfExist;

@Controller
@RequestMapping("/registration.htm")
public class RegistrationController {
	//variables to store in Redis
	
	private BigInteger gamalP;
	private BigInteger gamalG;
	private BigInteger gamalY;
	private BigInteger secretKey;
	
	@ModelAttribute
	public void setVaryResponseHeader(HttpServletResponse response) {
	    response.setHeader("Content-Type", "text/plain;charset=UTF-8");
	}
	
	@RequestMapping(method = GET)
	 public String form(Model model) {
		    return "pages/registration.html";
	}
	
	@RequestMapping(method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	public String route(@RequestBody String clientDataJson,Model model) {
			
			System.out.println(clientDataJson);
			
			SecureRandom random = new SecureRandom();
			byte bytes[] = new byte[20];
			random.nextBytes(bytes);
			String token = bytes.toString();
			UUID uuid = UUID.randomUUID();
		    String userID = uuid.toString();
		    
		    UserNavigation.createUserNavigation(userID);
		    UserNavigation.setRegistrationToken(userID, token);
		   
		    System.out.println("Registration 1 uuid:" + userID);
		    System.out.println("Registration 1 key:" + UserNavigation.getRegistrationToken(userID));
			
			ElGamal EG = new ElGamal();
			gamalP = EG.getpKey();
			gamalG = EG.getbKey();
			gamalY = EG.getcKey();
			secretKey = EG.getSecretKey();
			
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode node = mapper.createObjectNode();
			node.put("key",token);
			node.put("uuid",userID);
			node.put("gamalP", gamalP.toString());
			node.put("gamalG", gamalG.toString());
			node.put("gamalY", gamalY.toString());
			System.out.println("-P:" + gamalP + "-G:" + gamalG + "-Y:" + gamalY );
		
			return node.toString();
		
    }
	
	@RequestMapping(value="usr.htm",method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	public String register(@RequestBody String userDataJSON, Model model, 
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) throws 
											JsonProcessingException, IOException {
		
			ObjectMapper resultMapper = new ObjectMapper();
		    ObjectNode resultNode= resultMapper.createObjectNode();
		    
		    System.out.println("Registration 2 uuid:" + uuid);
		    System.out.println("Registration 2 key:" + key);
		    
		    if(UserNavigation.compareRegistrationToken(uuid, key)) {
		
				ObjectMapper mapper = new ObjectMapper();		
				User newUser = mapper.readValue(userDataJSON, User.class);
				String userName = DecriptElGamalString.decriptString(newUser.getName(), secretKey, gamalP);
				String userPassword = DecriptElGamalString.decriptString(newUser.getPassword(), secretKey, gamalP);
				System.out.println(userName + "--" + userPassword);
				byte[] encriptedUserName = EncryptHash.encriptSHA1(userName);
				byte[] encriptedPassword = EncryptHash.encriptSHA1(userPassword); 
				
				System.out.println("BYTE:" + encriptedUserName);
				System.out.println("BYTE:" + encriptedPassword);
	
				CheckUserIfExist cuie = new CheckUserIfExist(encriptedUserName);
				if(!cuie.check()) {
					CreateNewUser cne = new CreateNewUser(encriptedUserName, encriptedPassword);
					if(cne.create()) {
						
						SecureRandom random = new SecureRandom();
						byte bytes[] = new byte[20];
						random.nextBytes(bytes);
						String tokenNew = bytes.toString();
						UUID uuidNew = UUID.randomUUID();
					    String userIDNew = uuidNew.toString();
					    
					    UserNavigation.createUserNavigation(userIDNew);
					    UserNavigation.setRegistrationToken(userIDNew, tokenNew);
					    UserNavigation.setUserName(userIDNew, encriptedUserName);
					    UserNavigation.deleteUser(uuid);
					    UserNavigation.setUserNameString(userIDNew, userName);
						
					    resultNode.put("key", tokenNew);
					    resultNode.put("uuid", userIDNew);
						resultNode.put("result", 1);
						
					} else {
						
						resultNode.put("result", "Server error...");
						
					}
				} else {
					
					resultNode.put("result", "User alredy exists!");
					
				}
		    } else {
		    	resultNode.put("result", "False request...");
		    }
			
			return resultNode.toString();	
   }
	
	
}
