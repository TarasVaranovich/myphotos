package myphotos.web;


import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@EnableWebMvc
@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(Exception.class)
	public void redisConnectionJavaException(Exception e) {
		
	    System.out.println("HANDLER:" + e.toString());
	    
	}
}
