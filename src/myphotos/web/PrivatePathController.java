package myphotos.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/errors.htm")
public class PrivatePathController {
	
	@ModelAttribute
	public void setVaryResponseHeader(HttpServletResponse response) {
		response.setHeader("Content-Type", "text/plain;charset=UTF-8");
	}
	
	@RequestMapping(method = GET)
	 public String form(Model model) {
		    return "pages/wrongpage.html";
	}
}
