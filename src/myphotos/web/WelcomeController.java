package myphotos.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/welcome.htm")
public class WelcomeController {
	
	@RequestMapping(method = GET)
	 public String form(Model model) {
		    return "pages/welcome.html";
	}

}
