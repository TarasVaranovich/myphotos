package myphotos.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class TempFilesFilter implements Filter {
	
	 public void init(FilterConfig filterConfig) throws ServletException {
	    }

	    public void doFilter(ServletRequest request, 
	    					ServletResponse response, 
	    					FilterChain chain) throws IOException, ServletException {
	    	
	    	HttpServletResponse httpResponse = (HttpServletResponse) response;
	    	
	        httpResponse.sendRedirect("/myphotos/errors.htm");

	    }

	    public void destroy() {

	    }
	    
}