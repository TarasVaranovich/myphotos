package myphotos.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.IOException;
import java.security.SecureRandom;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import myphotos.data.DeleteDocumentByID;
import myphotos.data.GetDocumentByOrder;
import myphotos.navigation.UserNavigation;
import myphotos.navigation.UserNavigationSafety;
import myphotos.sequrity.EncryptHash;
import myphotos.struct.Document;
import myphotos.struct.DeleteDocuments;

@Controller
@RequestMapping("/documents.htm")
public class DocumentsController {
	
	private String documentsUUIDTmp;	
	//Integer documentsCounter = 0;
	//variables to store in Redis
	String token;
	byte[] userNameTmp = EncryptHash.encriptSHA1("Taras");
		
	@ModelAttribute
	public void setVaryResponseHeader(HttpServletResponse response) {
		response.setHeader("Content-Type", "text/plain;charset=UTF-8");
	}
	
	@RequestMapping(method = GET)
	public String photosForm(Model model,
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) {
		
		System.out.println("Document get request 1:" + uuid);
		System.out.println("Document get request 2:" + key);
		
		//documentsCounter = 0;//reset documents counter
		
		
		if(UserNavigation.comparePhotosToken(uuid, key)) {
			
			this.documentsUUIDTmp = uuid;
			
			return "pages/documents.html";
			
		} else {
			
			return "pages/wrongpage.html";
			
		}
	}
	
	@RequestMapping(value="usr.htm",method = POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String documentsUserKey(Model model) {		
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		token = bytes.toString();
		
	    ObjectMapper mapperUser = new ObjectMapper();
	    ObjectNode userNode = mapperUser.createObjectNode();
	    userNode.put("token",token);
	    userNode.put("uuid",this.documentsUUIDTmp);
	    userNode.put("name", UserNavigation.getUserNameString(this.documentsUUIDTmp));
	    
	    UserNavigation.setDocumentsToken(this.documentsUUIDTmp, token);
	    
		return userNode.toString();
	}
	
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	@RequestMapping(value="getdocuments.htm",method = GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String documentsGetContent(Model model, 
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key,
			@RequestHeader(value="documentnumber") Integer documentNumber) {	
		
		System.out.println("Get documents UUID:" + uuid);
		System.out.println("Get documets key:" + key);
		UserNavigationSafety uns = new UserNavigationSafety();
		ObjectMapper mapperDocument = new ObjectMapper();
		ObjectNode documentNode = mapperDocument.createObjectNode();
		
		
		//documentsCounter ++;
		
		if(uns.compareDocumentsToken(uuid, key)) {
			GetDocumentByOrder gdbo= new GetDocumentByOrder(uns.getUserName(uuid), documentNumber);
			Document currentDoc = gdbo.getDocument();	
			if(currentDoc != null) {
				
				documentNode.put("docname",currentDoc.getName());
				documentNode.put("docbin", currentDoc.getBinary());
				documentNode.put("docdate", currentDoc.getDate());
				documentNode.put("docid", currentDoc.getID());
				
				return documentNode.toString();
				
			} else {
				System.out.println("Document is null");
				return "";
			}
		} else {
			return "";
		}	
	}
	
	@RequestMapping(value="deletedocuments.htm",method = POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String deletePhotos(@RequestBody String documentsJson, Model model, 
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) {
		System.out.println("Delete documents UUID:" + uuid);
		System.out.println("Delete documents key:" + key);
		Boolean deleteMarker = true;
		ObjectMapper deletePhotosMapper = new ObjectMapper();
		
		if(UserNavigation.compareDocumentsToken(uuid, key)) {
			try {
				DeleteDocuments newDeleteDocuments = deletePhotosMapper.readValue(documentsJson, 
						DeleteDocuments.class);
				for(Integer n = 0; n < newDeleteDocuments.getDocumentIDS().length; n++ ) {
					DeleteDocumentByID dfbid = new DeleteDocumentByID(newDeleteDocuments.getDocumentIDS()[n]);
					if(dfbid.delete()) {
						deleteMarker = true;
					} else {
						deleteMarker = false;
					}
				}
			} catch(IOException ioe) {
				
				ioe.printStackTrace(); 
			} 
		} else {
			deleteMarker = false;
		}
		System.out.println(documentsJson);
		return deleteMarker.toString();
	}
	
	@RequestMapping(value="exitfromdocuments.htm",method = GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String exitFromDocuments(Model model, 
			@RequestParam(value = "uuid", required = true) String uuid, 
			@RequestParam(value = "key", required = true) String key) { 
		Boolean exitMarker = false;
		System.out.println("Exit from documents uuid:" + uuid);
		System.out.println("Exit from documents server key:" + key);
		UserNavigationSafety uns = new UserNavigationSafety();
		if(uns.compareDocumentsToken(uuid, key)) {
			
			if(uns.deleteUser(uuid)) {
				System.out.println("User deleted. LOG OUT.");
				exitMarker = true;
			}
			
		} else {
			System.out.println("User NOT deleted.");
			exitMarker = false;
		}
		
		return exitMarker.toString();
	}
}
