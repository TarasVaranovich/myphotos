package myphotos.web;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.FilterChain;
import java.io.IOException;


public class PagesFilter implements Filter {
	
		public void init(FilterConfig filterConfig) throws ServletException {
	    
		}

	    public void doFilter(ServletRequest request, 
	    					ServletResponse response, 
	    					FilterChain chain) throws IOException, ServletException {
	    	
	    	HttpServletResponse httpResponse = (HttpServletResponse) response;
	    	
	    	httpResponse.sendRedirect("/myphotos/errors.htm");
	    	//hain.doFilter(request, response); // for exceptions test only
	    }

	    public void destroy() {
	    	
	    }
	    
}
