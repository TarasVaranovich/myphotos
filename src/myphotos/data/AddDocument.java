package myphotos.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.nio.file.Files;
import java.io.File;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


public class AddDocument {
	
	private String documentPath;
	private String documentName;
	private byte[] userName;
	
	public AddDocument(String documentPath,String documentName,byte[] userName) {
		
		this.documentPath = documentPath;
		this.documentName = documentName;
		this.userName = userName;
	}
	
	public Boolean Add () {
		Boolean add = false;
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
			return false;
		}

		Connection mySQLconn = null;
		
		try {
			mySQLconn = DriverManager.getConnection(DBConnection.getDBPath(),
													DBConnection.getDBUser(),
													DBConnection.getDBPassword());
			
			System.out.println("Connection for new document creation success.");
			String query = 
					"SELECT id FROM usr WHERE BINARY name= BINARY ?";
			PreparedStatement pstmt = mySQLconn.prepareStatement(query);
			
			//INSERT INTO photo(usrid, name, content, format, description, originDate) VALUES(?, ?, ?, ?, ?, ?, ?)
			
			pstmt.setBytes(1, this.userName);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			//System.out.println("User id:" + rs.getInt(1));
			Integer userId = rs.getInt(1);
			
			 //yyyy/MM/dd
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();	
			String currentDate = dateFormat.format(date);
			//--add	
			byte[] documentBinary = Files.readAllBytes(new File(this.documentPath).toPath());	
			String addQuery = "INSERT INTO document(usrid, name, content, originDate) VALUES(?, ?, ?, STR_TO_DATE( ?, '%c/%e/%Y %r'))";
			PreparedStatement pstmtAdd = mySQLconn.prepareStatement(addQuery);
			pstmtAdd.setInt(1, userId);
			pstmtAdd.setString(2, this.documentName);
			pstmtAdd.setBytes(3, documentBinary);
			pstmtAdd.setString(4, currentDate); 	
			pstmtAdd.executeUpdate();
			add = true;
			
			//end add
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return false;
		}
		
		return add;
	}
}
