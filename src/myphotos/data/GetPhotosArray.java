package myphotos.data;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;

import myphotos.struct.Photo; 

public class GetPhotosArray {
	
	private Photo[] photos;
	private byte[] userName;
	
	public GetPhotosArray(Photo[] photos,byte[] userName) {
		this.photos = photos;
		this.userName = userName;
	}	
	
	public void createPhotos() {
		for(Integer z = 0; z< this.photos.length; z++) {
			this.createPhoto(this.photos[z]);
		}
	}
	
	public void createPhoto(Photo photo) {
		
		String sourceData =  photo.getBinary();
		String[] parts = sourceData.split(",");
		String imageString = parts[1];
		byte[] imageByte = Base64.getDecoder().decode(imageString);
		BufferedImage image = null;
		File outputfile = new File("/Users/Taras/Desktop/myphotos/myphotos/WebContent/tempfiles/"+ photo.getName() +"."+ photo.getFormat());
		try {
			
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();
			ImageIO.write(image, photo.getFormat(), outputfile);
			String filePath = outputfile.getAbsolutePath();
			System.out.println("->>File created:" + filePath);
			
		}catch(IOException e) {
			
			System.out.println(e.getMessage());
			
		}
	}
}
