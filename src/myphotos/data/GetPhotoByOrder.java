package myphotos.data;

import myphotos.struct.PhotoDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Base64;

import myphotos.data.DBConnection;

public class GetPhotoByOrder {
	
	private byte[] userName;
	private Integer photoOrder;
	
	public GetPhotoByOrder (byte[] userName, Integer photoOrder) {
		this.userName = userName;
		this.photoOrder = photoOrder;
	}
	
	public PhotoDB getPhoto() {
		
		PhotoDB photo = new PhotoDB();
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		}

		Connection mySQLconn = null;
		try {
			mySQLconn = DriverManager.getConnection(DBConnection.getDBPath(),
													DBConnection.getDBUser(),
													DBConnection.getDBPassword());
			
			System.out.println("Connection for getting photos by order success.");
			String query = 
					"SELECT id FROM usr WHERE BINARY name= BINARY ?";
			PreparedStatement pstmt = mySQLconn.prepareStatement(query);
			pstmt.setBytes(1, this.userName);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			Integer userId = rs.getInt(1);
			String selectPhotoQuery = 
					"SELECT id, name, content, format, description, originDate FROM photo WHERE usrid="
			+userId +" LIMIT "+ photoOrder +", 1";
			PreparedStatement pstmtSelectPhoto = mySQLconn.prepareStatement(selectPhotoQuery);
	
			ResultSet photoRS = pstmtSelectPhoto.executeQuery();
			if(photoRS.next()) {
				photo.setName(photoRS.getString("name"));
				photo.setBinary(byteToBase64(photoRS.getBytes("content")));
				photo.setDate(photoRS.getDate("originDate").toString());
				photo.setDescription(photoRS.getString("description"));
				photo.setFormat(photoRS.getString("format"));
				photo.setID(photoRS.getInt("id"));
			} else {
				photo = null;
			}
			//close connections
			pstmt.close();
			pstmtSelectPhoto.close();
			mySQLconn.close();
			
		} catch (Exception e) {
			System.err.println(e);
		}
		
		
		return photo;
	}
	
	private String byteToBase64(byte[] data){
		
		String resultString = Base64.getEncoder().encodeToString(data);
		
		return resultString;
	}
}
