package myphotos.data;

import myphotos.struct.Photo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Base64;

import myphotos.data.DBConnection;

public class AddPhotos {
	
	private Photo[] photos;
	private byte[] userName;
	
	public AddPhotos(Photo[] photos,byte[] userName) {
		this.photos = photos;
		this.userName = userName;
	}	
	
	public Boolean add() {
		
		Boolean add = false;
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
			return false;
		}

		Connection mySQLconn = null;
		
		try {
			mySQLconn = DriverManager.getConnection(DBConnection.getDBPath(),
													DBConnection.getDBUser(),
													DBConnection.getDBPassword());
			
			System.out.println("Connection for new photos creation success.");
			String query = 
					"SELECT id FROM usr WHERE BINARY name= BINARY ?";
			PreparedStatement pstmt = mySQLconn.prepareStatement(query);
			
			//INSERT INTO photo(usrid, name, content, format, description, originDate) VALUES(?, ?, ?, ?, ?, ?, ?)
			
			pstmt.setBytes(1, this.userName);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			//System.out.println("User id:" + rs.getInt(1));
			Integer userId = rs.getInt(1);
			
			//--add
			for(Integer z = 0; z < this.photos.length; z ++) {
			Photo photo	 = this.photos[z];
			byte[] photoBinary = base64ToBinaary(photo.getBinary());		
			String addQuery = "INSERT INTO photo(usrid, name, content, format, description, originDate) VALUES(?, ?, ?, ?, ?, STR_TO_DATE( ?, '%c/%e/%Y %r'))";
			PreparedStatement pstmtAdd = mySQLconn.prepareStatement(addQuery);
			pstmtAdd.setInt(1, userId);
			pstmtAdd.setString(2, photo.getName());
			pstmtAdd.setBytes(3, photoBinary);
			pstmtAdd.setString(4, photo.getFormat());
			pstmtAdd.setString(5, photo.getDescription());
			pstmtAdd.setString(6, photo.getDate()); 	
			pstmtAdd.executeUpdate();
			add = true;
			}
			//end add
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return false;
		}
		
		return add;
	}
	
	private byte[] base64ToBinaary(String base64Str) {
			
			String[] parts = base64Str.split(",");
			String dataString = parts[1];
			byte[] dataByte = Base64.getDecoder().decode(dataString);
			
			return dataByte;
	}

}
