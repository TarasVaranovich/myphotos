package myphotos.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import myphotos.data.DBConnection;

public class CreateNewUser {
	
	private byte[] userName;
	private byte[] userPassword;
	
	public CreateNewUser (byte[] userName, byte[] userPassword) {
		
		this.userName = userName;
		this.userPassword = userPassword;
	}
	
	public Boolean create () {
		
		Boolean create = false;
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
			return false;
		}

		Connection mySQLconn = null;
		try {
			mySQLconn = DriverManager.getConnection(DBConnection.getDBPath(),
													DBConnection.getDBUser(),
													DBConnection.getDBPassword());
			
			System.out.println("Connection for new user creation success.");
			String query = 
					"INSERT INTO usr(name, password) VALUES(?, ?)";
			PreparedStatement pstmt = mySQLconn.prepareStatement(query);
			pstmt.setBytes(1, userName);
			pstmt.setBytes(2, userPassword);
			pstmt.execute();
			create = true;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return false;
		}
		
		return create;
	}

}
