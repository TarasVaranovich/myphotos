package myphotos.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import myphotos.data.DBConnection;

public class CheckUserIfExist {
	
	private byte[] userNameCompare;
	
	public CheckUserIfExist (byte[] userNameCompare) {
		
		this.userNameCompare = userNameCompare;
	}
	
	public Boolean check () {
		
		Boolean check = true;
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		}

		Connection mySQLconn = null;
		try {
			mySQLconn = DriverManager.getConnection(DBConnection.getDBPath(),
													DBConnection.getDBUser(),
													DBConnection.getDBPassword());
			
			System.out.println("Connection for chek username success.");
			String query = 
					"SELECT IF(EXISTS(SELECT * FROM usr WHERE BINARY name= BINARY ?),1,0);";
					
			PreparedStatement pstmt = mySQLconn.prepareStatement(query);
			pstmt.setBytes(1, userNameCompare);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			if (rs.getInt(1) == 0) {
				check = false;
			}
			
			
		} catch (Exception e) {
			System.err.println(e);
		}
		
		return check;
	}
	
}
