package myphotos.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import myphotos.data.DBConnection;

public class CheckUserPassword {
	
	private byte[] recievedUserName;
	private byte[] comparePassword;
	
	public CheckUserPassword(byte[] recievedUserName, byte[] comparePassword) {
		this.recievedUserName = recievedUserName;
		this.comparePassword = comparePassword;
	}
	
	public Boolean check () {
		
		Boolean check = true;
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		}

		Connection mySQLconn = null;
		try {
			mySQLconn = DriverManager.getConnection(DBConnection.getDBPath(),
													DBConnection.getDBUser(),
													DBConnection.getDBPassword());
			
			System.out.println("Connection for chek user password success.");
			String query = 
					"SELECT IF(EXISTS(SELECT * FROM usr WHERE BINARY name= BINARY ? AND BINARY password= BINARY ?),1,0);";
					
			PreparedStatement pstmt = mySQLconn.prepareStatement(query);
			pstmt.setBytes(1, recievedUserName);
			pstmt.setBytes(2, comparePassword);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			if (rs.getInt(1) == 0) {
				check = false;
			}
			
			
		} catch (Exception e) {
			System.err.println(e);
		}
		
		return check;
	}	
}
