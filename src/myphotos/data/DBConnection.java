package myphotos.data;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public final class DBConnection {
	
	private static final String dbPropsPath ="/Users/Taras/Desktop/myphotos/myphotos/resources/myphotos_db.properties";
	
	public static String getDBPath() {
		
		String dbPath = readProps("connectionPath");
		
		return dbPath;
	}
	
	public static String getDBUser() {
		
		String dbUser = readProps("user");
		
		return dbUser;
	}
	
	public static String getDBPassword() {
		
		String dbPassword = readProps("password");
		
		return dbPassword;
	}
	
	private static String readProps(String propsKey) {
		
		String property = "";
		
		Properties dbProps = new Properties();
		try {
			
			FileInputStream input = new FileInputStream(dbPropsPath);
			dbProps.load(input);
			property = dbProps.getProperty(propsKey);
			
			return property;
			
		} catch (IOException ioe) {
			
			ioe.printStackTrace();
			
			return null;
		}
	}
	

}
