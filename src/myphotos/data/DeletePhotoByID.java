package myphotos.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class DeletePhotoByID {
	
	private Integer photoID;
	
	public DeletePhotoByID(Integer photoID) {
		
		this.photoID = photoID;
	}
	
	public Boolean delete() {
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		}

		Connection mySQLconn = null;
		try {
			mySQLconn = DriverManager.getConnection(DBConnection.getDBPath(),
													DBConnection.getDBUser(),
													DBConnection.getDBPassword());
			
			System.out.println("Connection for delete photo by ID success.");
			
			String deletePhotoQuery = 
					"DELETE FROM photo WHERE id=" + photoID;
			PreparedStatement pstmtDeletePhoto = mySQLconn.prepareStatement(deletePhotoQuery);
	
			pstmtDeletePhoto.executeUpdate();
			//close connections
			pstmtDeletePhoto.close();
			mySQLconn.close();
			
			return true;
			
		} catch (Exception e) {
			System.err.println(e);
			
			return false;
		}
	}
}
