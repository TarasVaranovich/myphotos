package myphotos.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import myphotos.converters.PDFtoBase64Converter;

import myphotos.struct.Document;

public class GetDocumentByOrder {
	
	private byte[] userName;
	private Integer documentOrder;
	
	public GetDocumentByOrder(byte[] userName, Integer documentOrder) {
		this.userName = userName;
		this.documentOrder = documentOrder;
	}
	
	public Document getDocument() {
		
		Document document = new Document();
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		}

		Connection mySQLconn = null;
		try {
			mySQLconn = DriverManager.getConnection(DBConnection.getDBPath(),
													DBConnection.getDBUser(),
													DBConnection.getDBPassword());
			
			System.out.println("Connection for getting documents by order success.");
			String query = 
					"SELECT id FROM usr WHERE BINARY name= BINARY ?";
			PreparedStatement pstmt = mySQLconn.prepareStatement(query);
			pstmt.setBytes(1, this.userName);
			ResultSet rs = pstmt.executeQuery();
			
			rs.next();
			Integer userId = rs.getInt(1);
			String selectDocumentQuery = 
						"SELECT id, name, content, originDate FROM document WHERE usrid="
								+userId +" LIMIT "+ documentOrder +", 1";
			PreparedStatement pstmtSelectDocument = mySQLconn.prepareStatement(selectDocumentQuery);
		
			ResultSet documentRS = pstmtSelectDocument.executeQuery();
			
			if(documentRS.next()) {
				document = new Document(documentRS.getString("name"), 
						PDFtoBase64Converter.convertFromBytes(documentRS.getBytes("content")), 
						documentRS.getDate("originDate").toString(),
						documentRS.getInt("id"));
			} else {
				document = null;
			}
			//close connections
			pstmt.close();
			pstmtSelectDocument.close();
			mySQLconn.close();
			
		} catch (Exception e) {
			System.err.println(e);
		}
		return document;
	}
}
