package myphotos.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import myphotos.struct.PhotoBin;


public class GetPhotoByID {
	
	private Integer photoID;
	
	public GetPhotoByID(Integer photoID) {
		
		this.photoID = photoID;
	}
	
	public PhotoBin getPhotoBin() {
		
		PhotoBin photobin = new PhotoBin();
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		}

		Connection mySQLconn = null;
		try {
			mySQLconn = DriverManager.getConnection(DBConnection.getDBPath(),
													DBConnection.getDBUser(),
													DBConnection.getDBPassword());
			
			System.out.println("Connection for getting photo by ID success.");
			
			String selectPhotoQuery = 
					"SELECT name, content, format, description, originDate FROM photo WHERE id="
			+ photoID;
			PreparedStatement pstmtSelectPhoto = mySQLconn.prepareStatement(selectPhotoQuery);
	
			ResultSet photoRS = pstmtSelectPhoto.executeQuery();
			photoRS.next();
			photobin.setName(photoRS.getString("name"));
			photobin.setImageBinary(photoRS.getBytes("content"));
			photobin.setDate(photoRS.getDate("originDate").toString());
			photobin.setDescription(photoRS.getString("description"));
			photobin.setFormat(photoRS.getString("format"));
			//close connections
			pstmtSelectPhoto.close();
			mySQLconn.close();
			
		} catch (Exception e) {
			System.err.println(e);
		}
		
		return photobin;
	}
}
