package myphotos.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class DeleteDocumentByID {
	
	private Integer documentID;
	
	public DeleteDocumentByID(Integer documentID) {
		
		this.documentID = documentID;
	}
	
	public Boolean delete() {
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		}

		Connection mySQLconn = null;
		try {
			mySQLconn = DriverManager.getConnection(DBConnection.getDBPath(),
													DBConnection.getDBUser(),
													DBConnection.getDBPassword());
			
			System.out.println("Connection for delete document by ID success.");
			
			String deleteDocumentQuery = 
					"DELETE FROM document WHERE id=" + documentID;
			PreparedStatement pstmtDeleteDocument = mySQLconn.prepareStatement(deleteDocumentQuery);
	
			pstmtDeleteDocument.executeUpdate();
			//close connections
			pstmtDeleteDocument.close();
			mySQLconn.close();
			
			return true;
			
		} catch (Exception e) {
			System.err.println(e);
			
			return false;
		}
	}

}
