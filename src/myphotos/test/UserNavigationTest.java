package myphotos.test;


import java.security.SecureRandom;
import java.util.UUID;

import myphotos.navigation.UserNavigation;
import myphotos.sequrity.EncryptHash;
import java.util.Arrays;

public class UserNavigationTest {
	public static void main(String[] args) {
		long beginMilles = System.currentTimeMillis() % 1000;
        System.out.println("->Begin execution of User Navigation Test:" +  beginMilles);
        
        //BEGIN
        UUID usrID = UUID.randomUUID();
        String user = usrID.toString();
        System.out.println("User ID:" + user);
        
        SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		byte trueBytes[] = bytes;
		String trueRandom = trueBytes.toString();
		System.out.println("True Bytes:" + trueRandom);
		
		SecureRandom random2 = new SecureRandom();
		byte bytes2[] = new byte[20];
		random2.nextBytes(bytes2);
		byte falseBytes[] = bytes2;	
		String falseRandom =falseBytes.toString();
		System.out.println("False Bytes:" + falseRandom);
		
		//
		UserNavigation.createUserNavigation(user);
		UserNavigation.setLoginToken(user, trueRandom);
		System.out.println("True Bytes from DB:" + UserNavigation.getLoginToken(user));
		
		UserNavigation.createUserNavigation(user);
		UserNavigation.setLoginToken(user, falseRandom);
		System.out.println("False Bytes from DB:" + UserNavigation.getLoginToken(user));
		
		//
		System.out.println("Compare Values:");
		if(UserNavigation.compareLoginToken(user, falseRandom)) {
			System.out.println("True compare");
		}
		if(!UserNavigation.compareLoginToken("mm", falseRandom)) {
			System.out.println("True false user compare");
		}
		if(!UserNavigation.compareLoginToken(user, trueRandom)) {
			System.out.println("Fase value compare");
		}
		
		byte[] userNameTmp = EncryptHash.encriptSHA1("Taras");
		System.out.println("Bytes before Redis:" + new String(userNameTmp));
		UserNavigation.setUserName(user, userNameTmp);
		byte[] userNameTmp2 = UserNavigation.getUserName(user);
		System.out.println("Bytes from Redis:" + new String(UserNavigation.getUserName(user)));
		
		if(Arrays.equals(userNameTmp, userNameTmp2)) {
			System.out.println("Equal!");
		}
		UserNavigation.deleteUser(user);
		//END
		
        long endMilles = System.currentTimeMillis() % 1000;
        System.out.println("->End execution of User Navigation Test:" +  endMilles);
        
    }
}
