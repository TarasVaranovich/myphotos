package myphotos.converters;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;


public class HTMLtoPDFConverter {
	
	public HTMLtoPDFConverter() {
		
	}
	
	public static Boolean convert(String HTMLPath, String PDFPath) {
		
		File file = new File(PDFPath);
		
		try{
			Document document = new Document();
	        // step 2
	        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
	        writer.setInitialLeading(12);
	        // step 3
	        document.open();
	        // step 4
	        XMLWorkerHelper.getInstance().parseXHtml(writer, document,
	                new FileInputStream(HTMLPath));
	        // step 5
	        document.close();
	        
		} catch(IOException ioe) {
			ioe.printStackTrace();
			return false;
		} catch (DocumentException de) {
			de.printStackTrace();
			return false;
		}
		
		return true;
	}
}
