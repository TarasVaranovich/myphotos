package myphotos.converters;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;

public class PDFtoBase64Converter {
	
	public static String convert(String pdfDocPath) {
		String pdfStr = "";
		File file = new File(pdfDocPath);
		try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            pdfStr = "data:application/pdf;base64," + Base64.getEncoder().encodeToString(bytes);
            fileInputStreamReader.close();
            
        } catch (FileNotFoundException e) {
    
            e.printStackTrace();
        } catch (IOException e) {
            
            e.printStackTrace();
        }
		return pdfStr;
	}
	
	public static String convertFromBytes(byte[] bytesArray) {
		String pdfStr = "data:application/pdf;base64," + Base64.getEncoder().encodeToString(bytesArray);
		return pdfStr;
	}
}
