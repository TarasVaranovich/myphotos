package myphotos.navigation;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class RedisInstance {
	
	private static final String rediPropsPath ="/Users/Taras/Desktop/myphotos/myphotos/resources/myphotos_redis.properties";
	
	public static String getHost() {
		
		String redisHost = readProps("redisHost");
		
		return redisHost;
	}
	
	public static Integer getPort() {
		
		Integer redisPort =Integer.valueOf(readProps("redisPort")).intValue();
		
		return redisPort;
	}
	
	public static Integer getTimeout() {
		
		Integer redisTimeout =Integer.valueOf(readProps("redisTimeOut")).intValue();
		
		return redisTimeout;
	}
	
	public static Integer getKeyTTL() {
		
		Integer redisTimeout =Integer.valueOf(readProps("keyTTL")).intValue();
		
		return redisTimeout;
	}
	
	private static String readProps(String propsKey) {
		
		String property = "";
		
		Properties dbProps = new Properties();
		try {
			
			FileInputStream input = new FileInputStream(rediPropsPath);
			dbProps.load(input);
			property = dbProps.getProperty(propsKey);
			
			return property;
			
		} catch (IOException ioe) {
			
			ioe.printStackTrace();
			
			return null;
		}
	}
}
