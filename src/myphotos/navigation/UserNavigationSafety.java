package myphotos.navigation;

import java.util.Base64;
import java.util.Map;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class UserNavigationSafety {
	
	private Jedis jedis = createJedisPool().getResource();
	
	public UserNavigationSafety() {
		
	}
	
	public Boolean comparePhotosToken(String user, String givenPhotosToken) {
	
		try {
		//Jedis jedis = createJedisPool().getResource();
		
			if(jedis.exists(user)) {
				System.out.println("User exists true from compare photos token.");
				Map<String, String> properties = jedis.hgetAll(user);
				String photosToken = properties.get("photosToken");
				if(photosToken.equals(givenPhotosToken)) {
					//jedis.close();
					return true;
				} else {
					//jedis.close();
					return false;
				}
			} else {
				//jedis.close();
				return false;
			}
		} catch(JedisConnectionException jce) {
			System.out.println("Safety EX:" + jce.toString());
			return false;
		}	
	}
	
	public Boolean compareDocumentsToken(String user, String givenDocumentsToken) {	
		try {
			if(jedis.exists(user)) {
				Map<String, String> properties = jedis.hgetAll(user);
				String documentsToken = properties.get("documentsToken");
				if(documentsToken.equals(givenDocumentsToken)) {
					//jedis.close();
					return true;
				} else {
					
					//jedis.close();
					return false;
				}
			} else {
				
				//jedis.close();
				return false;
			}	
		} catch(JedisConnectionException jce) {
			System.out.println("Safety EX:" + jce.toString());
			return false;
		}
	}
	
	public Boolean compareAddPhotosToken(String user, String givenAddPhotosToken) {
		
		if(jedis.exists(user)) {
			Map<String, String> properties = jedis.hgetAll(user);
			String addPhotosToken = properties.get("addPhotosToken");
			if(addPhotosToken.equals(givenAddPhotosToken)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}		
	}

	public byte[] getUserName(String user) {
		
		Map<String, String> properties = jedis.hgetAll(user);
		String userNameStr = properties.get("userName");
		byte[] userNameByte = Base64.getDecoder().decode(userNameStr);
		//jedis.close();
		
		return userNameByte;
	}
	
	public Boolean deleteUser (String user) {
		jedis.del(user);
		return true;
	}

	private JedisPool createJedisPool() {
		JedisPoolConfig config = new JedisPoolConfig();
		JedisPool pool = new JedisPool(config, RedisInstance.getHost(), RedisInstance.getPort());
		
		return pool;
	}
}
