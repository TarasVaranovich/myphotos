package myphotos.navigation;

import java.util.Map;
import java.util.HashMap;
import java.util.Base64;

import myphotos.navigation.RedisInstance;

import redis.clients.jedis.Jedis;

public class UserNavigation {
	
	private static final Jedis jedis = new Jedis(RedisInstance.getHost(), 
											RedisInstance.getPort(), 
											RedisInstance.getTimeout());
	public static Boolean createUserNavigation(String user) {
		
		Map<String, String> navigationProperties = new HashMap<String, String>();
		navigationProperties.put("loginToken", "");
		navigationProperties.put("registrationToken", "");
		navigationProperties.put("photosToken", "");
		navigationProperties.put("addPhotosToken", "");
		navigationProperties.put("documentsToken", "");
		navigationProperties.put("pdfPreviewToken", "");
		navigationProperties.put("userName", "");
		navigationProperties.put("userNameString", "");
		
		jedis.hmset(user, navigationProperties);
		jedis.expire(user,RedisInstance.getKeyTTL());
		System.out.println("User Key TTL:" + jedis.ttl(user));
		System.out.println("User Key PTTL (from init):" + jedis.pttl(user));
		return true;
	}
	
	//set values
	public static Boolean setLoginToken(String user, String loginToken){
		jedis.hset(user, "loginToken", loginToken);
		return true;
	}
	
	public static Boolean setRegistrationToken(String user, String registrationToken){
		jedis.hset(user, "registrationToken", registrationToken);
		return true;
	}
	
	public static Boolean setPhotosToken(String user, String photosToken){
		jedis.hset(user, "photosToken", photosToken);
		return true;
	}
	
	public static Boolean setAddPhotosToken(String user, String addPhotosToken){
		jedis.hset(user, "addPhotosToken", addPhotosToken);
		return true;
	}
	
	public static Boolean setDocumentsToken(String user, String documentsToken){
		jedis.hset(user, "documentsToken", documentsToken);
		return true;
	}
	
	public static Boolean setPdfPreviewToken(String user, String pdfPreviewToken){
		jedis.hset(user, "pdfPreviewToken", pdfPreviewToken);
		return true;
	}
	
	public static Boolean setUserName(String user, byte[] userNameByte){
		String userName = Base64.getEncoder().encodeToString(userNameByte);
		jedis.hset(user, "userName", userName);
		return true;
	}
	
	public static Boolean setUserNameString(String user, String userNameString) {
		jedis.hset(user, "userNameString", userNameString);
		return true;
	}
	//get values
	public static String getLoginToken(String user) {
		Map<String, String> properties = jedis.hgetAll(user);
		String loginToken =  properties.get("loginToken");
		return loginToken;
	}
	
	public static String getRegistrationToken(String user) {
		Map<String, String> properties = jedis.hgetAll(user);
		String registrationToken =  properties.get("registrationToken");
		return registrationToken;
	}
	
	public static String getPhotosToken(String user) {
		Map<String, String> properties = jedis.hgetAll(user);
		String photosToken = properties.get("photosToken");
		return photosToken;
	}
	
	public static String getAddPhotosToken(String user) {
		Map<String, String> properties = jedis.hgetAll(user);
		String addPhotosToken = properties.get("addPhotosToken");
		return addPhotosToken;
	}
	
	public static String getDocumentsToken(String user) {
		Map<String, String> properties = jedis.hgetAll(user);
		String documentsToken = properties.get("documentsToken");
		return documentsToken;
	}
	
	public static String getPdfPreviewToken(String user) {
		Map<String, String> properties = jedis.hgetAll(user);
		String pdfPreviewToken = properties.get("pdfPreviewToken");
		return pdfPreviewToken;
	}
	
	public static byte[] getUserName(String user) {
		Map<String, String> properties = jedis.hgetAll(user);
		String userNameStr = properties.get("userName");
		byte[] userNameByte = Base64.getDecoder().decode(userNameStr);
		return userNameByte;
	}
	
	public static String getUserNameString(String user) {
		Map<String, String> properties = jedis.hgetAll(user);
		String userNameString = properties.get("userNameString");
		return userNameString;
	}
	//compare values
	
	public static Boolean compareLoginToken(String user, String givenLoginToken) {
		
		if(jedis.exists(user)) {
			Map<String, String> properties = jedis.hgetAll(user);
			String loginToken = properties.get("loginToken");
			if(loginToken.equals(givenLoginToken)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}		
	}
	
	public static Boolean compareRegistrationToken(String user, String givenRegistrationToken) {
		
		if(jedis.exists(user)) {
			Map<String, String> properties = jedis.hgetAll(user);
			String registrationToken = properties.get("registrationToken");
			if(registrationToken.equals(givenRegistrationToken)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}		
	}
	
	public static Boolean comparePhotosToken(String user, String givenPhotosToken) {
		
		if(jedis.exists(user)) {
			Map<String, String> properties = jedis.hgetAll(user);
			String photosToken = properties.get("photosToken");
			if(photosToken.equals(givenPhotosToken)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}		
	}
	
	public static Boolean compareAddPhotosToken(String user, String givenAddPhotosToken) {
		
		if(jedis.exists(user)) {
			Map<String, String> properties = jedis.hgetAll(user);
			String addPhotosToken = properties.get("addPhotosToken");
			if(addPhotosToken.equals(givenAddPhotosToken)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}		
	}
	
	public static Boolean compareDocumentsToken(String user, String givenDocumentsToken) {
		
		if(jedis.exists(user)) {
			Map<String, String> properties = jedis.hgetAll(user);
			String documentsToken = properties.get("documentsToken");
			if(documentsToken.equals(givenDocumentsToken)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}		
	}
	
	public static Boolean comparePdfPreviewToken(String user, String givenPdfPreviewToken) {
		
		if(jedis.exists(user)) {
			Map<String, String> properties = jedis.hgetAll(user);
			String pdfPreviewToken = properties.get("pdfPreviewToken");
			if(pdfPreviewToken.equals(givenPdfPreviewToken)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}		
	}
	//delete
	public static Boolean deleteUser (String user) {
		jedis.del(user);
		return true;
	}

}
