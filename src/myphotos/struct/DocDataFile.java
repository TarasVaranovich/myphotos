package myphotos.struct;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public final class DocDataFile {
	
	private static final String jsonDocPropsPath ="/Users/Taras/Desktop/myphotos/myphotos/resources/myphotos_tempfiles.properties";
	
	public static String getPath() {
		
		String jsonDocDataPath = readProps("jsonDocDataPath");
		
		return jsonDocDataPath;
	}
	
	public static String getDocDescPath() {
		
		String jsonDocDescPath = readProps("jsonDocDescPath");
		
		return jsonDocDescPath;
	}
	
	public static String getHTMLtempletePath() {
		
		String HTMLtempletePath = readProps("htmlTempletePath");
		
		return HTMLtempletePath;
	}
	
	public static String getPDFDocumentPath() {
		
		String PDFDocumentPath = readProps("pdfDocumentPath");
		
		return PDFDocumentPath;
	}
	
	public static String getImageInstancePath() {
		
		String imageInstancePath = readProps("imageInstansePath");
		
		return imageInstancePath;
	}
	
	public static String getTempFilesPath() {
		
		String tempFilesPath = readProps("tempFilesPath");
		
		return tempFilesPath;
	}
	
	private static String readProps(String propsKey) {
		
		String property = "";
		
		Properties dbProps = new Properties();
		try {
			
			FileInputStream input = new FileInputStream(jsonDocPropsPath);
			dbProps.load(input);
			property = dbProps.getProperty(propsKey);
			
			return property;
			
		} catch (IOException ioe) {
			
			ioe.printStackTrace();
			
			return null;
		}
	}
}
