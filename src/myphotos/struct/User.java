package myphotos.struct;


public class User {
	
	private String password;
	private String name;
	
	public User(String password, String name){
		this.password = password;
		this.name = name;
	}
	public User() {
		
	}
	
	public String getName() {
		return name;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
}
