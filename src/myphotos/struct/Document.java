package myphotos.struct;

public class Document {
	
	private String name;
	private String binary; 
	private String date; 
	private Integer id;
	
	public Document() {
		
	}
	
	public Document (String name, String binary, String date, Integer id) {
		this.name = name;
		this.binary = binary;
		this.date = date;
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getBinary() {
		return this.binary;
	}
	
	public String getDate() {
		return this.date;	
	}
	
	public Integer getID() {
		return this.id;
	}
}
