package myphotos.struct;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import myphotos.struct.PhotoBin;

public class CreateDocumentTemplete {
	
	private List<PhotoBin> photos = new ArrayList<PhotoBin>();
	private String docName;
	private String uuid;
	
	public CreateDocumentTemplete(List<PhotoBin> photos, String docName, String uuid) {
		this.photos = photos;
		this.docName = docName;
		this.uuid = uuid;
	}
	
	public Boolean create() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(DocDataFile.getTempFilesPath() 
																						+ uuid
																						+ ".html"));
			bw.write("<html><head><meta charset='UTF-8'/><title>Result Page</title></head><body style=\"font-family:tahoma\">");
			bw.write("<div style=\"background:rgb(230,230,230); padding:5px ;border:2px solid black;\">" +
			this.docName + 
  			"</div>");
			//create images
			for(Integer i = 0; i < this.photos.size(); i++) {
				
				Photo photo = this.photos.get(i);
				
				bw.write("<div style=\"float:left; width:160px;\">");
				bw.write("<img style =\"float:left\" height=\"200px\" width= \"160px\" src=\"" + 
					DocDataFile.getTempFilesPath() +
					photo.getName() + 
					"-" +
					uuid + 
					"." +
					photo.getFormat() + 
					"\" title=\"" +
					photo.getName() +
					"\"/>");
				bw.write("<p>" + 
					photo.getName() +
					"</p>");
				bw.write("<p>" + 
					photo.getDate() +
					"</p>");
				bw.write("<p>" + 
					photo.getDescription() +
					"</p>");
				bw.write("</div>");
			}
			//end create images
			bw.write("<div style=\"background:rgb(230,230,230); width:100% ; height:10px; border:2px solid black;\"></div>");
			bw.write("</body></html>");
			bw.close();
			return true;
		} catch(IOException ioe) {
			System.out.println("Document HTML failure:" + ioe);
			return false;
		}
	}
}
