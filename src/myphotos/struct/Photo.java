package myphotos.struct;

public class Photo {
	
	private String name;
	private String binary; // binary
	private String format;
	private String description;
	private String date; // datetime
	
	public Photo() {
		
	}
	
	public Photo(String name, 
			 	 String binary, 
			 	 String format, 
			 	 String description,
			 	 String date) {
		
		this.name = name;
		this.binary = binary;
		this.format = format;
		this.description = description;
		this.date = date;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getBinary() {
		return this.binary;
	}
	
	public String getFormat() {
		return this.format;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getDate() {
		return this.date;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setBinary(String binary) {
		this.binary = binary;
	}
	
	public void setFormat(String format) {
		this.format = format;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
}
