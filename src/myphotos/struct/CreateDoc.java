package myphotos.struct;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;


import java.io.IOException;

import java.util.List;
import java.lang.Exception;

import javax.imageio.ImageIO;

import java.util.ArrayList;



import myphotos.data.GetPhotoByID;

import myphotos.struct.DocDataFile;
import myphotos.struct.CreateDocumentTemplete;


public class CreateDoc {
	
	private String uuid;
	private String docName;
	private Integer photoIDS[];
	
	List<PhotoBin> photos = new ArrayList<PhotoBin>();
	
	public CreateDoc() {
		
	}
	
	public CreateDoc(String docName,Integer photoIDS[]) { 
		this.docName = docName;
		this.photoIDS = photoIDS;
		
	}
	
	public void setUUID(String uuid) {
		this.uuid = uuid;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	
	public void setPhotoIDS(Integer photoIDS[]) {
		this.photoIDS = photoIDS;
	}
	
	public String getDocName() {
		return this.docName;
	} 
	
	public Integer[] getPhotoIDS() {
		return this.photoIDS;
	}
	
	public Boolean createImages() {
		
		try {			
			for(Integer k = 0; k < photoIDS.length; k++) {
				GetPhotoByID gfbi = new GetPhotoByID(photoIDS[k]);
				PhotoBin photobin = gfbi.getPhotoBin();		
				this.createImageInstance(photobin.getImageBinary(), 
												photobin.getName(),
												photobin.getFormat());	
				photobin.setImageBinary(null);
				photos.add(photobin);
			}
			return true;
			
		} catch(Exception e) {
			return false;
		}
		
	}
	
	public Boolean createHTMLPage() {
		CreateDocumentTemplete cdt = new CreateDocumentTemplete(this.photos, this.docName, this.uuid);
		if(cdt.create()) {
			return true;
		} else {
			return false;
		}
	}
	
	private void createImageInstance(byte[] imageBinryData, String imageName, String imageFormat) {
		
		File outputfile = new File(DocDataFile.getTempFilesPath() + 
															imageName + 
															"-" +
															this.uuid +
															"." + 
															imageFormat);
		BufferedImage image = null;
		try {
			
		ByteArrayInputStream bis = new ByteArrayInputStream(imageBinryData);
		image = ImageIO.read(bis);
		bis.close();
		ImageIO.write(image, imageFormat, outputfile);
		String filePath = outputfile.getAbsolutePath();
		System.out.println("->>File created:" + filePath);
		}catch(IOException e) {
			
			System.out.println(e.getMessage());
			
		}
		
	}
	
	public void deleteTempFiles() {
		//File htmlFile = new File(DocDataFile.getHTMLtempletePath());
		//htmlFile.delete();
		File dir = new File(DocDataFile.getTempFilesPath());
		File[] files = dir.listFiles();
		for(File f: files) {
			String extension = f.getName().substring(f.getName().lastIndexOf(".") + 1);
			if(!extension.equals("pdf")) {
				f.delete();
			}
		}
	}
	
}
