package myphotos.struct;

public class DeleteDocuments {
	
	private Integer documentIDS[];
	
	public DeleteDocuments() {
		
	}
	
	public DeleteDocuments(Integer documentIDS[]) {
		this.documentIDS = documentIDS;
	}
	
	public Integer[] getDocumentIDS () {
		return this.documentIDS;
	}
	
	public void setDocumentIDS (Integer documentIDS[]) {
		this.documentIDS = documentIDS;
	}	
}
