package myphotos.struct;

import myphotos.struct.Photo;

public class PhotoBin extends Photo {
	
	private byte[] imagebinary;
	
	public void setImageBinary(byte[] imagebinary) {	
		this.imagebinary = imagebinary;	
	}
	
	public byte[] getImageBinary() {
		return this.imagebinary;
	}

}
