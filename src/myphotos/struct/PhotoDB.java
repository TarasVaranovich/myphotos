package myphotos.struct;

import myphotos.struct.Photo;

public class PhotoDB extends Photo{
	
	private Integer id;
	
	public void setID(Integer id) {
		this.id = id;
	}
	
	public Integer getID() {
		return this.id;
	}
	
}
