package myphotos.struct;

public class DeletePhotos {
	
	private Integer photoIDS[];
	
	public DeletePhotos() {
		
	}
	
	public DeletePhotos(Integer photoIDS[]) {
		this.photoIDS = photoIDS;
	}
	
	public Integer[] getPhotoIDS () {
		return this.photoIDS;
	}
	
	public void setPhotoIDS (Integer photoIDS[]) {
		this.photoIDS = photoIDS;
	}
}
