package myphotos.config;

import org.springframework.web.WebApplicationInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.servlet.DispatcherServlet;
import javax.servlet.ServletRegistration;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import myphotos.web.PagesFilter;
import myphotos.web.TempFilesFilter;

public class AppInitializer implements WebApplicationInitializer {
	
	@Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        WebApplicationContext context = getContext();
        servletContext.addListener(new ContextLoaderListener(context));
        //ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
        // for exception handling
        DispatcherServlet dispatcherServlet = new DispatcherServlet(context);
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);   
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", dispatcherServlet);
        // end for exception handling*/
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("*.htm");
        servletContext.addFilter("Pages filter", PagesFilter.class).addMappingForUrlPatterns(null, false, "/pages/*");
        servletContext.addFilter("Temp files filter", TempFilesFilter.class).addMappingForUrlPatterns(null, false, "/tempfiles/*");
    }

    private AnnotationConfigWebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation("myphotos.web");
        return context;
    }
 
}
