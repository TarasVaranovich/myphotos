package myphotos.math;

public class GCD {

	public static int gcd(int a,int b) {
		
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        System.out.println("GCD->" + a);
        return a;
    }
	
}
