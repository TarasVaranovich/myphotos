package myphotos.math;

import java.util.ArrayList;
import java.util.List;

import myphotos.math.SimpleDeviders;
import myphotos.math.CoprimeIntegers;

public class PrimitiveRootModulo {
	
	public List<Integer> getPrimitives (Integer p) {
		
		 List<Integer> primitives = new ArrayList<Integer>();
		 
		 Integer eulersTotientFunction = p-1;
		 
		 SimpleDeviders sd = new SimpleDeviders();
		 
		 List<Integer> simpleDeviders = sd.getSimpleDeviders(p-1);
		 
		 CoprimeIntegers ci = new CoprimeIntegers();
		 
		 List<Integer> comprimeIntegers = ci.getCoprimeIntegers(p);
		 
		 for(Integer g = 0; g < comprimeIntegers.size(); g++) {
			 
			 Boolean marker = true;
			 Integer gCandidate = comprimeIntegers.get(g);
			 
			 for(Integer q = 0; q < simpleDeviders.size(); q++) {
				 
				 if(Math.pow(gCandidate,eulersTotientFunction/simpleDeviders.get(q)) % p == 1) {
	
					 System.out.println("FALSE:" + gCandidate);
					 
					 marker = false;
					 
					 break;
					
				 } else {
					 System.out.println("TRUE?:" + gCandidate + ":" + Math.pow(gCandidate,eulersTotientFunction/simpleDeviders.get(q)) % p);
				 }
				 
			 }
			 
			 /*Double secOp = Math.pow(g, eulersTotientFunction) % p;
			 if(secOp == 1){
				 for(Integer i=1; i < eulersTotientFunction;i++ ) {
					 
					 Double firstOp = Math.pow(g, i) % p;
					 					 
					 if(firstOp == 1) {
						 marker = false;
						 break;
					 }
					 
				 }
			 }*/
			 
			 if(marker) {
				 primitives.add(gCandidate);
			 }
			
		 }
		 
		return primitives;
	}

}
