package myphotos.math;

import java.util.ArrayList;
import java.util.List;

import myphotos.math.GCD;

public class CoprimeIntegers {
	
	public List<Integer> getCoprimeIntegers(Integer n) {
		
		List<Integer> coprimeIntegers = new ArrayList<Integer>();
		
		
		for (Integer i = 0; i < n; i++) {
			
			Integer z = GCD.gcd(i, n);
			
			if(z == 1) {
				
				coprimeIntegers.add(i);
				System.out.println("Coprime Integer ->" + i);
			}
			
		}
		
		return coprimeIntegers;
	}
}
