package myphotos.math;

import java.util.List;
import java.util.ArrayList;
public class SieveOfErathosthenes {
	
	public List<Integer> getPrimes (Integer n) {
	
	        Boolean[] is_prime = new Boolean[n+1];
	        for (int i = 2; i <= n; ++i) 
	            is_prime[i] = true;
	
	        List<Integer> primes = new ArrayList<Integer>();
	
	        for (int i = 2; i <= n; ++i)
	            if (is_prime[i]) {
	                primes.add(i);
	                if (i * i <= n)
	                    for (int j=i*i; j<=n; j+=i)
	                        is_prime[j] = false;
	            }
	
	        return primes;
	        
	}
}
