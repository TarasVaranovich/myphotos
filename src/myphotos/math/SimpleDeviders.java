package myphotos.math;

import java.util.ArrayList;
import java.util.List;

import myphotos.math.SieveOfErathosthenes;

public class SimpleDeviders {
	
	public List<Integer> getSimpleDeviders(Integer n) {
		
		List<Integer> simpleDeviders = new ArrayList<Integer>();
		SieveOfErathosthenes soe = new SieveOfErathosthenes();
		List<Integer> simpleNumbers = soe.getPrimes(n);
		
		for(Integer i = 0; i < simpleNumbers.size(); i++) {
			
			if(n % simpleNumbers.get(i) == 0){
				
				simpleDeviders.add(simpleNumbers.get(i));
				System.out.println("Simple devider ->" + simpleNumbers.get(i));
			}
		}
		
		return simpleDeviders;
	}

}
